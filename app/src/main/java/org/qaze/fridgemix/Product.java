package org.qaze.fridgemix;

public class Product {
    String name;
    int amount;

    public Product(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    public String toString() {
        return name + " " + Integer.toString(amount);
    }

}

package org.qaze.fridgemix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class FridgeActivity extends AppCompatActivity {

    ArrayList<Product> productList = new ArrayList<>();
    ArrayList<String> showList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fridge);

        loadFridge();
        ListView listView = (ListView) findViewById(R.id.listView);
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, showList);
        listView.setAdapter(arrayAdapter);
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = FridgeActivity.this.getAssets().open("list.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void loadFridge() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("fridge");

            showList.add("");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);

                String name = jo_inside.getString("name");
                Integer amount = jo_inside.getInt("amount");

                Product product = new Product(name, amount);
                productList.add(product);
                showList.add(product.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package org.qaze.fridgemix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MotionEvent;


public class MainActivity extends AppCompatActivity {

    float x_up, x_down, y_up, y_down;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void twojStary(View view)
    {
        Intent intent = new Intent(this, search_activity.class);
        startActivity(intent);
    }

    public void showFridge(View view) {
        Intent intent = new Intent(this, FridgeActivity.class);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar, menu);
        return true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchEvent) {
        switch (touchEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                x_down = touchEvent.getX();
                y_down = touchEvent.getY();

                break;
            case MotionEvent.ACTION_UP:
                x_up = touchEvent.getX();
                y_up = touchEvent.getY();
                if (x_up < x_down) {
                    //Intent i = new Intent(this, Search.class);
                    //startActivity(i);
                    //overridePendingTransition(R.anim.enter_right, R.anim.exit_right);
                } else if (x_up > x_down) {
                    //Intent i = new Intent(this, Search.class);
                    //startActivity(i);
                    //overridePendingTransition(R.anim.enter_left, R.anim.exit_left);
                }
                break;
        }
        return false;
    }
}
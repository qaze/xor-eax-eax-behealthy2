package org.qaze.fridgemix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import ir.mirrajabi.searchdialog.SimpleSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.BaseSearchDialogCompat;
import ir.mirrajabi.searchdialog.core.SearchResultListener;
import ir.mirrajabi.searchdialog.core.Searchable;

public class search_activity extends AppCompatActivity {

    public ArrayList<Item> items = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_activity);
        ListView listView = (ListView) findViewById(R.id.listView);
        final ArrayList<String> strings = new ArrayList<>();
        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,strings);
        listView.setAdapter(arrayAdapter);
        findViewById(R.id.btnSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SimpleSearchDialogCompat(search_activity.this, "Search",
                        "Co szukasz kutasiarzu", null, initdata(),
                        new SearchResultListener<Searchable>() {
                            @Override
                            public void onSelected(BaseSearchDialogCompat baseSearchDialogCompat, Searchable searchable, int i) {
                                items.add(new Item(searchable.getTitle()));
                                strings.add(searchable.getTitle());
                                arrayAdapter.notifyDataSetChanged();
                                baseSearchDialogCompat.dismiss();
                            }
                        }).show();
            }
        });

    }

    private ArrayList<SearchModel> initdata() {
        ArrayList<SearchModel> items = new ArrayList<>();
        items.add(new SearchModel("Twoja stara"));
        items.add(new SearchModel("Twoja stara zapierdala"));
        items.add(new SearchModel("Twoja stara opierdala"));
        items.add(new SearchModel("Twoja stara ciongie pale"));
        items.add(new SearchModel("Twoja stara to huj"));
        items.add(new SearchModel("Pomidory"));
        items.add(new SearchModel("Jajka"));
        items.add(new SearchModel("Jarmuz"));
        items.add(new SearchModel("Kawa"));
        items.add(new SearchModel("Herbata"));
        items.add(new SearchModel("SPERMA XDDDD!!!"));
        return items;
    }
}
